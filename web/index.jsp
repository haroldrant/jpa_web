<%@page import="Modelo.Departamento"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="es" dir="ltr">

    <head>
        <meta charset="utf-8">
        <title>DAO & DTO</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"
                integrity="sha512-RXf+QSDCUQs5uwRKaDoXt55jygZZm2V++WUZduaU/Ui/9EGp3f/2KZVahFZBKGH0s774sd3HmrhUy+SgOFQLVQ=="
        crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/styles.css"/>
        
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <div class="navbar-header">
                    <h1>DAO & DTO</h1>
                </div>
            </div>
        </nav>
        <br>
        <!-- Registro -->
        <div class="container">
            <h1>DEPARTAMENTOS Y MUNICIPIOS</h1>
            <div class="float-end"><a href="index.jsp">
                    <h3><i class="fas fa-home"></i></h3>
                </a></div><br>
            <div class="row mt-5">
                <div class="col-md-4">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5>Departamento</h5>
                            <form action="CreateDpto.do" method="post">
                                <div class="form-group">
                                    <input type="text" name="id" placeholder="Id Departamento" class="form-control" min="0">
                                </div>
                                <br>
                                <div class="form-group">
                                    <input type="text" name="nombre" placeholder="Nombre Departamento" class="form-control">
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary">Crear Departamento</button>
                            </form>
                        </div>
                    </div>
                    <jsp:include page="ShowDpto.do"/>
                    <div class="card text-center">
                        <div class="card-body">
                            <h5>Municipio</h5>
                            <form action="CreateMun.do" method="post">
                                <div class="form-group">
                                    <input type="text" name="nombre" placeholder="Nombre Municipio" class="form-control">
                                </div>
                                <br>
                                <select class="form-select form-select-sm" aria-label=".form-select-sm example"
                                        name="departamento">
                                    <%
                                        List<Departamento> dpto = (List) request.getSession().getAttribute("dptos");
                                        for(Departamento d: dpto){
                                    %>
                                    <option><%=d.getId_dpto() + " " + d.getNombre()%></option>
                                    
                                    <%
                                        }
                                    %>
                                </select>
                                <br>
                                <button type="submit" class="btn btn-primary">Crear Municipio</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Tabla -->
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link" href="jsp/dpto.jsp">Departamentos</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="jsp/mun.jsp">Municipios</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-6 offset-4">
                        <h5>
                            Harold Rueda - 1151904
                            | Jefferson Pallares - 1151508 <br>
                            UFPS-2021-Programación web - UFPS</h5>
                    </div>
                </div>
            </div>
        </footer>

    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
    crossorigin="anonymous"></script>

</html>